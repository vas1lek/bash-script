#!/bin/bash

for i in Первый Второй Третий Четвертый Пятый Шестой 
do
    echo $i сильвер
done

# из файла
file="list"  # указываем файл
IFS=$'\n' # указываем перевод строки в качестве разделителя
for i in $(cat $file)
do
    echo $i сильвер
done

#----------------------------------------
for file in /home/cloudadmin/*
do
    if [ -d $file ]   # -d file Проверяет, существует ли файл, и является ли он директорией.
    then
        echo "$file это папка"
    elif [ -f $file ]
    then
        echo "$file это файл"
    fi
done

#---------------------------------------
for ((i=1; i<10; i++))
do 
    echo "Номер $i"
done
# в одну строку
for ((i=1; i<10; i++)); do echo "Номер $i"; done
#--------------------------------------

for ((t=10; t>0; t--))
do 
    sleep 1
    echo "Это обратный отсчет $t"
done

#-------------------------------------
for ((x=1; x<=3; x++))
do
    echo "Начать цикл с номером $x"
    for ((y=1; y<=3; y++))
    do 
        echo "Это внутренний цикл с номером $y"
    done
done
#--------------------------------------
var1=11
while [ $var1 -gt 0 ]  # как только значение дошло до 0 цикл прерывается 
do 
    echo $var1
    var1=$[var1-1]
done
--------------------------------------

for var1 in 1 2 3 4 5 6 7 8 9 10
do 
    if [ $var1 -eq 5 ]   # -eq - равно
    then    
        break
    fi
    echo "Цикл работает посколькоу сейчас значения меньше 5 и равно $var1"
done

#больше 5 меньше 15

for ((i = 1; i < 20; i++))
do 
    if [ $i -gt 5 ] && [ $i -lt 15 ]  # -gt - больше, -lt - меньше 
    then continue
    fi
    echo $i
done

# Боевой скрипт
IFS=:
for FOLDER in $PATH
do  
    echo "$FOLDER:" >> log.txt
    for file in $FOLDER/*.*
    do  
        if [ -x $file ]   # -x file Проверяет, существует ли файл, и является ли он исполняемым.
        then    
            echo " $file" >> log.txt
        fi
    done 
done
# Доступность сервисов
for i in {7..9}  # последовательность
do 
    if ping -c 1 10.0.12.$i > /dev/null
    then    
        echo "Хост 10.0.12.$i - доступен"
    else 
        echo "Хост 10.0.12.$i - :("
    fi
done 
