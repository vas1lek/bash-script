#!/bin/bash

for i in 10 20 30 40 50 60
do 
    if ping -c 1 10.0.12.$i > /dev/null
    then    
        echo "Хост 10.0.12.$i - доступен"
    else 
        echo "Хост 10.0.12.$i - :("
    fi
done 